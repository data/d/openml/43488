# OpenML dataset: Stock-Information

https://www.openml.org/d/43488

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Information about more than 4600 companies tradable on Robinhood website
Source
The information is scraped from Robinhood website
Inspiration
The dataset contains useful information about Public Companies are being traded in US Stock market. Information about company size, market cap, PE ratio, list date, etc. is provided which can provide some insights about stock market in the US.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43488) of an [OpenML dataset](https://www.openml.org/d/43488). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43488/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43488/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43488/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

